import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Cliente from './componentes/Cliente'
import Gerenciamento_clientes from './componentes/Tabela_clientes'
import Altera_cliente from './componentes/Altera_cliente'
import Tabela from './componentes/Tabela_Funcionario'
import Login from './componentes/Login'
import Cadastro from './componentes/Cadastro'
import Produto from './componentes/Produto'
import Produto_Estoque from './componentes/Produto_Estoque'
import Altera_Estoque from './componentes/Altera_Estoque'
import Altera_cadastro from './componentes/Altera_cadastro'
export default function Routes() {



    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route path="/tabela" exact component={Tabela} />
                <Route path="/Cadastro" exact component={Cadastro} />
                <Route path="/cliente" exact component={Cliente} />
                <Route path="/produto" exact component={Produto}/>
                <Route path="/estoque" exact component={Produto_Estoque}/>
                <Route path="/alteraestoque/:id" exact component={Altera_Estoque}/>
                <Route path="/gerenciamento_clientes" exact component={Gerenciamento_clientes} />
                <Route path="/alteracliente/:id" exact component={Altera_cliente} />
                <Route path="/alteracadastro/:id" exact component={Altera_cadastro} />
            </Switch>
        </BrowserRouter>
    )
}