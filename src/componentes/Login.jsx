import React from 'react'
import './Login.css'

const initialState = {
    user: { CPF: '', password: '' }
}

export default class Cadastro extends React.Component {

    state = { ...initialState }


    clear(event) {
        event.preventDefault()
        this.setState({ user: initialState.user })
    }

    save(event) {
        event.preventDefault()
        const user = this.state.user
        this.setState({ user: initialState.user })
        console.log(user)


    }


    updateField(event) {

        const user = { ...this.state.user }
        user[event.target.name] = event.target.value
        this.setState({ user })
    }


    render() {
        return (
            <body className='login'>
                    <h2 className="title">Smartfluxo</h2>
                    <form className="Formlogin">
                        <div className='nome'>
                            <label  >CPF:</label>
                            <input type="text" name="cpf" value={this.state.user.cpf} onChange={e => this.updateField(e)} />
                        </div>
                        <div className='senha'>
                            <label >Senha:</label>
                            <input type="Password" name="password" value={this.state.user.password} onChange={e => this.updateField(e)} />
                        </div>
                        <div className='botoes'>
                            <button className="register" onClick={e => this.save(e)}>Entrar</button>

                            <button className="cancel" onClick={e => this.clear(e)}>Cancelar</button>
                        </div>
                    </form>
            </body>
        )}
}

