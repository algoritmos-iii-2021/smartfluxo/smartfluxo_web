import React from 'react'
import axios from 'axios'
import './Altera_cliente.css'


const initialState = {
    produto: { descricao: '', tipo: '', quantidade_estoque: '', valor_compra: '', valor_venda: '', foto: '' }
}

export default class UserCrud extends React.Component {

    state = { ...initialState }

    clear(event) {
        event.preventDefault()
        this.setState({ produto: initialState.produto })
    }

    save(event) {
        event.preventDefault()
        const produto = this.state.produto
        const id = this.props.match.params.id
        console.log(id);
        axios.put(`http://localhost:3000/produto/${Number(id)}`, produto)
             .then(resp => {
                this.props.history.push("/estoque")
                this.setState({ produto: initialState.produto })
                console.log(resp.data)

            })
    }

    updateField(event) {

        const produto = { ...this.state.produto }
        produto[event.target.name] = event.target.value
        this.setState({ produto })
    }

    render() {
        return (

            <div>
                <div className="menu1">
                    <div >
                        <header className="navbar1"><center><h1>Smartfluxo</h1></center> </header>
                    </div>

                </div>
                <div className="formulario1">

                    <h2>Atualização de dados do Cliente</h2>
                    <form >
                        <div>
                            <label  >Tipo:</label>
                            <input className='input' type="text" name="descricao" value={this.state.produto.descricao} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >Descrição:</label>
                            <input className='input' type="email" name="tipo" value={this.state.produto.tipo} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >Unidades:</label>
                            <input className='input' type="text" name="quantidade_estoque" value={this.state.produto.quantidade_estoque} onChange={e => this.updateField(e)}required />
                        </div>
                        <div>
                            <label >Valor De Compra:</label>
                            <input className='input' type="text" name="valor_compra" value={this.state.produto.valor_compra} onChange={e => this.updateField(e)} required />
                        </div>
                        <div>
                            <label >Valor De Venda:</label>
                            <input className='input' type="text" name="valor_venda" value={this.state.produto.valor_venda} onChange={e => this.updateField(e)} required />
                        </div>
                        <div>
                            <label >URL da Foto do Produto</label>
                            <input className='input' type="text" name="foto" value={this.state.produto.foto} onChange={e => this.updateField(e)} required />
                        </div>

                        <div>
                            <button className="register" onClick={e => this.save(e)}>Atualizar</button>

                            <button className="cancel" onClick={e => this.clear(e)}>Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        )


    }
}



