import React from "react"
import './Menu.css'
import { SidebarData } from "./MenuData"

function Sidebar() {
    return (
        <body>
        <div className="menu">
            <div >
                <img src="https://iconape.com/wp-content/png_logo_vector/nycs-bull-trans-sf-std.png" className="img" />
                <header className="navbar"><center><h1>Smartfluxo</h1></center> </header>
            </div>
            <div className="Sidebar">
                <ul className="SidebarList">
                    {SidebarData.map((val, key) => {
                        return (
                            <li key={key}
                                className="row"
                                onClick={() => { window.location.pathname = val.link }}>
                                <div id="icon">
                                    {val.icon}
                                </div>
                                <div id="title">
                                    {val.title}
                                </div>
                            </li>
                        )
                    })}
                </ul>
            </div>
        </div>
        </body>
    )
}

export default Sidebar
