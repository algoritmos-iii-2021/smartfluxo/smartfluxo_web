import React from 'react'
import axios from 'axios'
import './Altera_cadastro.css'


const initialState = {
    user: { nome: '', email: '', cpf: '', nivel: '', senha: '' }
}

export default class UserCrud extends React.Component {

    state = { ...initialState }

    clear(event) {
        event.preventDefault()
        this.setState({ user: initialState.user })
    }

    save(event) {
        event.preventDefault()
        const user = this.state.user
        const id = this.props.match.params.id
        console.log(id);
        axios.put(`http://localhost:3000/funcionario/${Number(id)}`, user)
            .then(resp => {
                this.props.history.push("/tabela")
                this.setState({ user: initialState.user })
                console.log(resp.data)

            })
    }

    updateField(event) {

        const user = { ...this.state.user }
        user[event.target.name] = event.target.value
        this.setState({ user })
    }

    render() {
        return (

            <div>
                <div className="menu1">
                    <div >
                        <header className="navbar1"><center><h1>Smartfluxo</h1></center> </header>
                    </div>

                </div>
                <div className="formulario1">

                    <h2>Atualização de dados do Fúncionario</h2>
                    <form >
                        <div  >
                            <label  >Nome:</label>
                            <input className='input' type="text" name="nome" value={this.state.user.nome} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >e-mail:</label>
                            <input className='input' type="email" name="email" value={this.state.user.email} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >cpf:</label>
                            <input className='input' type="text" name="cpf" value={this.state.user.cpf} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >Nivel:</label>
                            <input className='input' type="text" name="nivel" value={this.state.user.nivel} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >Senha:</label>
                            <input className='input' type="Password" name="senha" value={this.state.user.senha} onChange={e => this.updateField(e)} />
                        </div>
                        <hr />
                        <div>
                            <button className="register" onClick={e => this.save(e)}>Cadastrar</button>

                            <button className="cancel" onClick={e => this.clear(e)}>Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        )


    }
}



