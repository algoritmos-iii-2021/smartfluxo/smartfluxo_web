import React from 'react'

import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';



export const SidebarData = [
    {
        title:"Cadastro De Funcionarios",
        icon: <AssignmentIndIcon/>,
        link:"/Cadastro"
    },

    {
        title:"Cadastro de Produtos",
        icon:<LocalOfferIcon />,
        link:"/Produto"
    },

    {
        title:"Estoque",
        icon:<AddShoppingCartIcon/>,
        link:"/estoque"
    },

    // {
    //     // title:"Faturamento de Caixa",
    //     // icon: <MonetizationOnIcon />,
    //     // link:"/fatura"
    // },

    {
        title:"Gerenciamento Dos Funcionarios",
        icon: <SupervisedUserCircleIcon />,
        link:"/Tabela"
    },
    {
        title:"Cadastro De Clientes",
        icon: <SupervisedUserCircleIcon />,
        link:"/cliente"
    },
    {
        title:"Gerenciamento De Clientes",
        icon: <SupervisedUserCircleIcon />,
        link:"/gerenciamento_clientes"
    },
]