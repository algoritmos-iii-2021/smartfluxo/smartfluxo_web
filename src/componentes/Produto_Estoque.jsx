import React from 'react';
import axios from 'axios';
import Menu from './Menu'
import "./Produto_Estoque.css"
import { BiTrash } from 'react-icons/bi'
import { GrDocumentUpdate } from 'react-icons/gr'


export default class PersonList extends React.Component {
    state = {
        persons: []
    }

    componentDidMount() {
        axios.get(`http://localhost:3000/produto/`)
            .then(res => {
                const persons = res.data;
                console.log(persons);
                this.setState({ persons });
            })
    }

    handleRemoverlinha = async (e) => {
        
        const response = await axios.delete(`http://localhost:3000/produto/${Number(e)}`)
        if (response.status === 204) {
            alert(`Produto Excluido`)            
        }
        this.setState(this.componentDidMount)


    }

    handleatualiza = async (e) => {
        console.log(e)
        this.props.history.push("/alteraestoque/" + Number(e))

    }
    render() {
        return (
            <div>
                <Menu />
                <table className='tabela'>
                    <thead>
                        <tr>
                            <th>Descrição</th>
                            <th>Tipo</th>
                            <th>Unidades</th>
                            <th>Valor de Compra</th>
                            <th>Valor de Venda</th>
                            <th>Imagem</th>
                            <th>Alterar/Excluir</th>
                        </tr>
                    </thead>
                    <tbody>

                        {this.state.persons.map(person =>
                            <tr>
                                <td>{person.descricao}</td>
                                <td>{person.tipo}</td>
                                <td>{person.quantidade_estoque}</td>
                                <td>{person.valor_compra}</td>
                                <td>{person.valor_venda}</td>
                                <td source= {person.foto} />
                                <td><GrDocumentUpdate onClick={(e) => this.handleatualiza(person.id, e)} />< BiTrash onClick={(e) => this.handleRemoverlinha(person.id, e)} /></td>
                            </tr>
                        )}

                    </tbody>
                </table>
            </div>
        )
    }
}