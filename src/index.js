import ReactDom from 'react-dom'
import React from 'react'
import { ConfigProvider } from 'antd'
import 'antd/dist/antd.css';
import ptBR from 'antd/lib/locale/pt_BR'

import App from './app'

ReactDom.render( <
    ConfigProvider locale = { ptBR } >
    <
    App / >
    <
    /ConfigProvider>,

    document.getElementById('root')
)