# SmartFluxo_web
### v.1.0.0
### (rev0)
# SmartFluxo_api

#### Fluxo de Versionamento

##### Vamos trabalhar com o git flow, para versionar os conteudos
###### Master - So vamos fazer o merge quando a versão estiver fechada ou final de sprint
###### Release - Quando um dos participantes do grupo estiver com baixa demanda de trabalho, vai ser trocado de equipe para virar tester.
###### Hotfix - Quando tiver bug na master, vamos puxar a versão da master para resolver.
###### Develop - Aqui que vamos fazer o desenvolvimento do codigo.
###### Feature/ - Quando for criado alguma funcionalidade vamos fazer o commit dentro dessa feature/

##### Fazendo o commit

###### Iniciando repositorio
```sh
git init
```
###### Trocando de branch da master -> develop
```sh
git checkout develop
```
###### Criando uma feature/ dentro da develop
```sh
git checkout -b feature/ nome da feature
```

###### Adicionando o arquivo que você trabalhou
```sh
git add nome_do_arquivo.js
```
###### Commitando o arquivo
```sh
git commit -a -m "Enviando o arquivo no qual faz tão função"
```
###### Enviando para o gitlab
```sh
git push --set-upstream https://gitlab.com/algoritmos-iii-2021/smartfluxo/smartfluxo_web feature/ nome da feature
```
###### Pedindo merge
```sh
git merge feautre/ nome da feature develop
```
#### Comandos interessantes pro uso do git.

###### Listando as branch's disponivel
```sh
git branch -a
```
#### <Sprint's>

##### Sprint01 - Inicio 09 - 03 - 2021 Até 16 - 03 - 2021
###### Assuntos trabalhados -> Foi liberado 7 dias para o pessoal estudar sobre o assunto - [x]

##### Sprint02 - Inicio 17 - 03 -2021 Até 30 - 03 - 2021
###### Assuntos trabalhados -> 
 - [ ] Responsavel = { }
 - [ ] Responsavel = { }
 - [ ] Responsavel = { }
 - [ ] Responsavel = { }
###### Correção de bugs relatados ->
 - [x] Não foi encontrado problemas.
##### Sprint02 - Inicio 31 - 03 -2021 Até 13 - 04 - 2021
 - [ ]
 - [ ]
 - [ ]
 - [ ]
###### Correção de bugs relatados ->
 - [x] Não foi encontrado problemas.



#### </Sprint's>

#### Rodando o codigo por outras pessoas
##### 1º Comando
##### 2º Comando
##### 3º Comando
##### 4º Comando
##### 5º Comando

